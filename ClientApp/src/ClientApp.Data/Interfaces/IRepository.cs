﻿using ClientApp.Interfaces;
using System.Collections.Generic;

namespace ClientApp.Interfaces
{
    public interface IRepository<T, T2> where T : IBaseEntity<T2>
    {
        T GetById(T2 id);
        IEnumerable<T> ListAll();
        IEnumerable<T> List(ISpecification<T> spec);
        T Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
