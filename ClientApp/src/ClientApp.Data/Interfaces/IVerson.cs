﻿using System;
namespace ClientApp.Interfaces
{
    public interface IVerson
    {
        DateTime? DatetimeCreated { get; set; }
        DateTime? DatetimeUpdated { get; set; }
        DateTime? DatetimeDeleted { get; set; }
        DateTime? DatetimeRestored { get; set; }
        bool Status { get; set; }
        byte[] RowVersion { get; set; }
    }
}
