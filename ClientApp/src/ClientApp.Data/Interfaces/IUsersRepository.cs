﻿using ClientApp.Data.Entities;
using ClientApp.Data.Models;
using System;
using System.Threading.Tasks;

namespace ClientApp.Interfaces
{
    public interface IUsersRepository : IRepository<Users, Guid>, IAsyncRepository<Users, Guid>
    {
        UserLogined Login(string UserName, string PassWord);
        void Dispose();
    }
}
