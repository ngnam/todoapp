﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClientApp.Interfaces
{
    public interface IAsyncRepository<T,T2>
        where T : IBaseEntity<T2>
    {
        Task<T> GetByIdAsync(T2 id);
        Task<List<T>> ListAllAsync();
        Task<List<T>> ListAsync(ISpecification<T> spec);
        Task<T> AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
    }
}
