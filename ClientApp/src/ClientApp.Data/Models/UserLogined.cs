﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientApp.Data.Models
{
    public class UserLogined
    {
        public bool isAuth { get; set; }
        public string TokenKey { get; set; }
        public int Time { get; set; }
    }
}
