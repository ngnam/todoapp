﻿using ClientApp.Data.Entities;

namespace ClientApp.Specifications
{
    public class UsersFilterSpecification : BaseSpecification<Users>
    {
        public UsersFilterSpecification(string query) : base(i => 
            (!string.IsNullOrEmpty(query) ? i.UserName.Contains(query) : string.IsNullOrEmpty(query)))
        {
        }
    }
}
