﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientApp.Data.Entities
{
    public class Users : BaseEntity<Guid>
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDay { get; set; }
        public string TokenKey { get; set; }
    }
}
