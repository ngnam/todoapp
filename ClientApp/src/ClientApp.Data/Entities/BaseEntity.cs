﻿using ClientApp.Interfaces;

namespace ClientApp.Data.Entities
{
    public abstract class BaseEntity<T> : IBaseEntity<T>
    {
        public T Id { get; set; }
    }
}
