﻿using ClientApp.Data.Entities;
using ClientApp.Data.Models;
using ClientApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApp.Infrastructure.Data
{
    public class UsersRepository : EfRepository<Users, Guid>, IUsersRepository
    {
        public UsersRepository(ClientAppDbContext dbContext) : base(dbContext)
        {

        }

        public UserLogined Login(string UserName, string PassWord)
        {
            if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(PassWord))
                return new UserLogined()
                {
                    isAuth = false,
                    TokenKey = "",
                    Time = 60
                };
            var login = _dbContext.Users.Any(b => b.UserName == UserName.Trim() && b.PassWord == PassWord.Trim());
            if (!login)
                return new UserLogined()
                {
                    isAuth = false,
                    TokenKey = "",
                    Time = 60
                };

            return new UserLogined()
            {
                isAuth = true,
                TokenKey = Guid.NewGuid().ToString(),
                Time = 3600
            };
        }
    }
}
