﻿using ClientApp.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ClientApp.Infrastructure.Data
{
    public partial class ClientAppDbContext : DbContext
    {
        public ClientAppDbContext(DbContextOptions<ClientAppDbContext> options) : base(options)
        {

        }
        public virtual DbSet<Users> Users { get; set; }

        private void ConfigureUsers(EntityTypeBuilder<Users> entity)
        {
            entity.ToTable("users");
            entity.HasKey(e => e.Id);
            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.Property(e => e.UserName).HasMaxLength(200);
            entity.Property(e => e.PassWord).HasColumnType("nvarchar(max)"); 
            entity.Property(e => e.FirstName).HasMaxLength(500);
            entity.Property(e => e.LastName).HasMaxLength(500);
            entity.Property(e => e.BirthDay);      
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {           
            modelBuilder.Entity<Users>(ConfigureUsers);
        }
    }
}
