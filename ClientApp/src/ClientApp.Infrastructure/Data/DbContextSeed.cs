﻿using ClientApp.Data.Entities;
using ClientApp.Infrastructure.Data;
using ClientApp.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class DbContextSeed
    {
        public static async Task SeedAsync(IHostingEnvironment appEnvironment,
            ClientAppDbContext dbContext,
            ILoggerFactory loggerFactory,
            int? retry = 0)
        {
            int retryForAvailability = retry.Value;
            try
            {
                // TODO: Only run this if using a real database
                await dbContext.Database.MigrateAsync();

                // init Nguoidung
                if (!dbContext.Users.Any())
                {
                    dbContext.Users.AddRange(GetPreconfiguredUsers());
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (System.Exception ex)
            {
                if (retryForAvailability < 10)
                {
                    retryForAvailability++;
                    var log = loggerFactory.CreateLogger<DbContextSeed>();
                    log.LogError(ex.Message);
                    await SeedAsync(appEnvironment, dbContext, loggerFactory, retryForAvailability);
                }
            }
        }

        private static IEnumerable<Users> GetPreconfiguredUsers()
        {
            return new List<Users>() {
                new Users() { Id = Guid.NewGuid(), FirstName = "admin1", LastName = "admin2", PassWord = "admin", UserName = "admin"  },
                new Users() { Id = Guid.NewGuid(), FirstName = "Nam0", LastName = "Nguyen", PassWord = "123", UserName = "namnguyen1"  },
                new Users() { Id = Guid.NewGuid(), FirstName = "Nam1", LastName = "Nguyen", PassWord = "123", UserName = "namnguyen2"  },
                new Users() { Id = Guid.NewGuid(), FirstName = "Nam2", LastName = "Nguyen", PassWord = "123", UserName = "namnguyen3"  },
                new Users() { Id = Guid.NewGuid(), FirstName = "Nam3", LastName = "Nguyen", PassWord = "123", UserName = "namnguyen4"  },
                new Users() { Id = Guid.NewGuid(), FirstName = "Nam4", LastName = "Nguyen", PassWord = "123", UserName = "namnguyen5"  },
                new Users() { Id = Guid.NewGuid(), FirstName = "Nam5", LastName = "Nguyen", PassWord = "123", UserName = "namnguyen6"  },
            };
        }
    }
}

