﻿using ClientApp.Services;
using ClientApp.Services.Dtos;
using ClientApp.Services.Dtos.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ClientApp.WebApp.Controllers
{
    [Route("api/[controller]")]
    [Authorize("Bearer")]
    public class UsersController : BaseController
    {
        private readonly IUsersService _usersService;
        private IConfiguration _configuration;
        public UsersController(IUsersService usersService, IConfiguration configuration)
        {
            this._usersService = usersService;
            this._configuration = configuration;
        }

        [HttpGet]
        public IActionResult GetAll(int skip = 0, int take = 10, string query = null)
        {
            var users = _usersService.GetUserItems(skip, take, query);
            return JsonParse(users);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            return JsonParse(_usersService.GetUser(id));
        }

        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]UserInputDto model)
        {
            if (!ModelState.IsValid && ModelState.ErrorCount > 0)
            {
                return JsonParse<UserDto>(null, ModelState);
            }
            var user = await _usersService.CreateOrUpdate(model);
            return JsonParse(user);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody]UserInputDto model)
        {
            if (!ModelState.IsValid && ModelState.ErrorCount > 0)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return JsonParse<UserDto>(null, ModelState, allErrors.Select(t => t.ErrorMessage).ToArray());
            }
            var user = await _usersService.CreateOrUpdate(model);
            return JsonParse(user);
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            return JsonParse(_usersService.Delete(id));
        }

        [AllowAnonymous]
        [HttpPost("/api/authenticate")]
        public IActionResult Login([FromBody]LoginDto model)
        {
            if (!ModelState.IsValid && ModelState.ErrorCount > 0)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return JsonParse<LoginDto>(null, ModelState, allErrors.Select(t => t.ErrorMessage).ToArray());
            }
            var logined = _usersService.LoginWith(model.UserName.Trim(), model.PassWord.Trim());
            // gen token 
            string strToken = string.Empty;
            if(logined.isAuth)
            {
               strToken = GenerateToken(logined, DateTime.Now.Add(TimeSpan.FromDays(1)));
            }
            
            return Json(JsonConvert.SerializeObject(new
            {
                isAuth = logined.isAuth,
                time = TimeSpan.FromMinutes(60).TotalSeconds,
                tokeyType = "Bearer",
                tokenKey = strToken
            }));
        }

        private string GenerateToken(UserLoginDto user, DateTime expires)
        {
            var handler = new JwtSecurityTokenHandler();

            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(user.TokenKey, "TokenAuth"),
                new[] {
                    new Claim("token", user.TokenKey)
                }
            );

            var keyByteArray = Encoding.ASCII.GetBytes(_configuration["TokenAuthentication:SecretKey"]);
            var signingKey = new SymmetricSecurityKey(keyByteArray);
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
                //new RSACryptoServiceProvider(2048).ExportParameters(true)), SecurityAlgorithms.RsaSha256Signature
                Subject = identity,
                Expires = expires,
                Audience = _configuration["TokenAuthentication:Audience"],
                Issuer = _configuration["TokenAuthentication:Issuer"],
                NotBefore = DateTime.Now.Subtract(TimeSpan.FromMinutes(60))
            });
            return handler.WriteToken(securityToken);
        }
    }
}
