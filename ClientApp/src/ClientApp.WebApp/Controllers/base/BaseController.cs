﻿using ClientApp.WebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace ClientApp.WebApp.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {

        }

        protected JsonResult JsonParse<T>(T json, dynamic modelState = null, params string[] errors)
        {
            return Json(new ListModelResponse<T>() { result = json, modelState = modelState, errors = errors });
        }
    }
}
