﻿namespace ClientApp.WebApp.Models
{
    internal interface IResponse
    {
        dynamic modelState { get; set; }
        string[] errors { get; set; }
    }

    internal interface IListModelResponse<TModel> : IResponse
    {
        TModel result { get; set; }
    }

    internal class ListModelResponse<TModel> : IListModelResponse<TModel>
    {
        public TModel result { get; set;}
        public dynamic modelState { get; set; }
        public string[] errors { get; set; }
    }
}
