﻿using System.Collections.Generic;

namespace ClientApp.Services.Dtos
{
    public interface IBaseIndexDto<T1, T2>
    {
        IEnumerable<T1> Items { get; set; }
        T2 PaginationInfo { get; set; }
    }
}
