﻿using System.Collections.Generic;

namespace ClientApp.Services.Dtos
{
    public abstract class BaseIndexDto<T1, T2> : IBaseIndexDto<T1, T2>
    {
        public IEnumerable<T1> Items { get; set; }
        public T2 PaginationInfo { get; set; }
    }
}
