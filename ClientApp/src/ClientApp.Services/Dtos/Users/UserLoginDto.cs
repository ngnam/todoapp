﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClientApp.Services.Dtos.Users
{
    public class UserLoginDto
    {
        public bool isAuth { get; set; }
        public string TokenKey { get; set; }
        public int Time { get; set; }
    }

    public class LoginDto
    {
        [Required]
        [StringLength(500)]
        public string UserName { get; set; }
        [Required]
        [StringLength(500)]
        public string PassWord { get; set; }
    }
}
