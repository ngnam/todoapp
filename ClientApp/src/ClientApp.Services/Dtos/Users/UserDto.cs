﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClientApp.Services.Dtos
{
    public class UserIndexDto : BaseIndexDto<UserDto, PaginationInfoDto>
    {
    }

    public class UserDto
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDay { get; set; }
        public string TokenKey { get; set; }

        public string FullName
        {
            get
            {
                string fullName = string.Empty;
                if (!string.IsNullOrEmpty(FirstName))
                    fullName += FirstName;
                if (!string.IsNullOrEmpty(LastName))
                    fullName += " " + LastName;
                return fullName;
            }
            private set { }
        }
    }

    public class UserInputDto
    {
        public Guid UserId { get; set; }
        [Required]
        [StringLength(500)]
        public string UserName { get; set; }
        [Required]
        [StringLength(500)]
        public string PassWord { get; set; }
        [Required]
        [StringLength(500)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(500)]
        public string LastName { get; set; }
        public DateTime? BirthDay { get; set; }
        public string TokenKey { get; set; }
    }
}
