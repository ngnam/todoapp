﻿namespace ClientApp.Services.Dtos
{
    public class PaginationInfoDto
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalPages { get; set; }
        public int TotalItems { get; set; }
        public string Previous { get; set; }
        public string Next { get; set; }
    }
}
