﻿using ClientApp.Services.Dtos;
using ClientApp.Services.Dtos.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClientApp.Services
{
    public interface IUsersService 
    {
        UserIndexDto GetUserItems(int skip, int take, string query);
        UserLoginDto LoginWith(string UserName, string PassWord);
        Task<UserDto> CreateOrUpdate(UserInputDto model);
        UserDto GetUser(Guid Id);
        Guid Delete(Guid Id);
    }
}
