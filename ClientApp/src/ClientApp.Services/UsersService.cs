﻿using ClientApp.Data.Entities;
using ClientApp.Interfaces;
using ClientApp.Specifications;
using ClientApp.Services.Dtos;
using ClientApp.Services.Dtos.Users;
using System;
using System.Linq;
using System.Threading.Tasks;
using ClientApp.Data.Models;
using System.Collections.Generic;

namespace ClientApp.Services
{
    public class UsersService : IUsersService
    {
        private readonly IAppLogger<UsersService> _logger;
        private readonly IUsersRepository _usersRepository;

        public UsersService(
            IAppLogger<UsersService> logger,
            IUsersRepository usersRepository)
        {
            this._logger = logger;
            this._usersRepository = usersRepository;
        }

        public UserIndexDto GetUserItems(int skip = 0, int take = 10, string query = null)
        {
            _logger.LogInformation("GetUserItems called");
            var fiterUsersSpec = new UsersFilterSpecification(query);
            var root = _usersRepository.ListAsync(fiterUsersSpec).Result;
            var totalItems = root.Count();
            var itemsOnPage = root
                .Skip(skip)
                .Take(take)
                .ToList();

            var vm = new UserIndexDto()
            {
                Items = ListUsersModelToListUserDto(itemsOnPage),
                PaginationInfo = CreatePaginationInfoViewModel(skip, take, totalItems, itemsOnPage.Count),
            };

            _logger.LogInformation("GetUserItems ended");
            return vm;
        }

        public UserLoginDto LoginWith(string UserName, string PassWord)
        {
            return CreateViewModelUserLogined(_usersRepository.Login(UserName, PassWord));
        }

        public async Task<UserDto> CreateOrUpdate(UserInputDto model)
        {
            try
            {
                var userInput = MapDtoToUsersModel(model);
                if (userInput.Id != Guid.Empty)
                {
                    var entity = GetEntity(userInput.Id);
                    if (entity != null)
                    {
                       await _usersRepository.UpdateAsync(entity);
                       return UsersModelToDto(entity);
                    }
                    return new UserDto();
                }
                else
                {
                    var user = await _usersRepository.AddAsync(userInput);
                    return UsersModelToDto(user);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public UserDto GetUser(Guid Id)
        {
            var user = GetEntity(Id);
            return UsersModelToDto(user);
        }

        public Guid Delete(Guid Id)
        {
            try
            {
                var entity = GetEntity(Id);
                if (entity != null)
                    _usersRepository.Delete(entity);
                else
                    return Guid.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return Id;
        }

        #region # private 
        private UserDto UsersModelToDto(Users user)
        {
            if (user == null)
                return null;
            // map data here
            return new UserDto()
            {
                UserId = user.Id,
                UserName = user.UserName,
                BirthDay = user.BirthDay,
                FirstName = user.FirstName,
                LastName = user.LastName,
                TokenKey = user.TokenKey,
            };
        }

        private List<UserDto> ListUsersModelToListUserDto(List<Users> lstuser)
        {
            if (lstuser.Count == 0) return new List<UserDto>();
            return lstuser.Select(user => UsersModelToDto(user)).ToList();
        }

        private UserDto UsersModelToDto(UserInputDto user)
        {
            // map data here
            return new UserDto()
            {
                UserId = user.UserId,
                BirthDay = user.BirthDay,
                FirstName = user.FirstName,
                LastName = user.LastName,
                TokenKey = user.TokenKey,
            };
        }

        private PaginationInfoDto CreatePaginationInfoViewModel(int skip, int take, int totalItems, int totalPages)
        {
            var itemModel = new PaginationInfoDto()
            {
                PageNumber = skip,
                PageSize = take,
                TotalItems = totalItems,
                TotalPages = totalPages
            };
            itemModel.Next = (itemModel.PageNumber == itemModel.TotalPages - 1) ? "is-disabled" : "";
            itemModel.Previous = (itemModel.PageNumber == 0) ? "is-disabled" : "";
            return itemModel;
        }

        private UserLoginDto CreateViewModelUserLogined(UserLogined model)
        {
            return new UserLoginDto()
            {
                isAuth = model.isAuth,
                Time = model.Time,
                TokenKey = model.TokenKey
            };
        }

        private Users MapDtoToUsersModel(UserInputDto modelDto)
        {
            return new Users()
            {
                Id = modelDto.UserId,
                FirstName = modelDto.FirstName,
                LastName = modelDto.LastName,
                BirthDay = modelDto.BirthDay,
                TokenKey = modelDto.TokenKey,
                PassWord = modelDto.PassWord,
                UserName = modelDto.UserName
            };
        }

        private Users GetEntity(Guid Id)
        {
            if (Id == Guid.Empty) return new Users();
            return _usersRepository.GetById(Id);
        }
        #endregion
    }
}
