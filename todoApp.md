# ToDoApp

## Development server

Run `npm run serve-dev` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
## Build

Run `npm run build-dev` to build the project. The build artifacts will be stored in the `dist/` directory.

### Documents 

* [Angular style guild](https://angular.io/guide/styleguide#overall-structural-guidelines)
* [Angular HttpInterceptor](https://angular.io/api/common/http/HttpInterceptor)
* [Angular jwt authentication](https://blog.angular-university.io/angular-jwt-authentication/)
* [Angular authentication](https://auth0.com/blog/angular-2-authentication/)
* [Angular2-jwt](https://github.com/auth0/angular2-jwt)
* [Angular authenticatiopn using the httpClient & HttpIntercaptors](https://ryanchenkie.com/angular-authentication-using-the-http-client-and-http-interceptors)
* [Fake Backend](http://jasonwatmore.com/post/2017/12/15/angular-5-mock-backend-example-for-backendless-development)


### scripts
```code
"scripts": {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build --prod",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e",
    "build-dev": "npm run build",
    "ngsw-config": "node_modules/.bin/ngsw-config dist src/ngsw-config.json",
    "serve-dev": "npm run build-dev && http-server dist -p 4200",
    "build-prod-ngsw": "ng build --prod && npm run ngsw-config",
    "serve-prod-ngsw": "npm run build-prod-ngsw && http-server dist -p 4200"
},
```

# demo
[Todo App - api fake](https://ngclient.github.io)

### Overall structural guidelines angular

```bash
<project-root>
│───.....
│
│───src/
│   ├───account/                                    # module Account
│   │   ├───login/
│   │   │   ├───login.component.html
│   │   │   ├───login.component.scss
│   │   │   ├───login.component.ts
│   │   │   └───login.service.ts
│   │   ├───account-routing.module.ts
│   │   ├───account.component.html
│   │   ├───account.component.scss
│   │   ├───account.component.ts
│   │   └───account.module.ts
│   ├───app/                                         # module app
│   │   ├───home/
│   │   │   ├───home.component.html
│   │   │   └───home.component.ts
│   │   ├───layout/
│   │   │   ├───aside/
│   │   │   │   ├───aside.component.html
│   │   │   │   ├───aside.component.scss
│   │   │   │   ├───aside.component.ts
│   │   │   │   └───menu-trees.service.ts
│   │   │   └───header/
│   │   │       ├───header.component.html
│   │   │       └───header.component.ts
│   │   ├───users/
│   │   │   ├───users.component.html
│   │   │   └───users.component.ts
│   │   ├───app-routing.module.ts
│   │   ├───app.component.html
│   │   ├───app.component.ts
│   │   └───app.module.ts
│   ├───assets/
│   │   ├───images/
│   │   │   └───logo.png
│   │   └───appconfig.json
│   ├───environments/
│   │   ├───environment.prod.ts
│   │   └───environment.ts
│   ├───shared/                                 # module shared
│   │   ├───auth/
│   │   │   ├───app-auth.service.ts
│   │   │   ├───auth-route-guard.ts
│   │   │   ├───token-service.ts
│   │   │   └───token.interceptor.ts            # token httpInterceptor  
│   │   ├───directives/
│   │   │   ├───click-inside.directive.ts
│   │   │   └───click-outside.directive.ts
│   │   ├───helpers/
│   │   │   └───json-helper.ts
│   │   ├───models/
│   │   │   ├───AuthModel.ts
│   │   │   └───user-model.ts
│   │   ├───page-not-found/
│   │   │   ├───page-not-found.component.html
│   │   │   └───page-not-found.component.ts
│   │   ├───service-proxies/
│   │   │   ├───fake-backend-interceptor.module.ts
│   │   │   ├───mockdata.todo.service.ts
│   │   │   ├───service-proxies.ts
│   │   │   └───service-proxy.module.ts
│   │   ├───utils/
│   │   │   └───utils-service.ts
│   │   ├───app-component-base.ts
│   │   ├───AppConsts.ts
│   │   ├───paged-listing-component-base.ts
│   │   └───shared.module.ts
│   ├───todo-theme/                 # theme fontend scss
│   │   ├───styles.scss
│   │   ├───_all.scss
│   │   ├───_animations.scss
│   │   ├───_layout.scss
│   │   ├───_mixin.scss
│   │   ├───_repository.scss
│   │   ├───_ultility.scss
│   │   └───_variables.scss
│   ├───AppPreBootstrap.ts          
│   ├───favicon.ico
│   ├───index.html
│   ├───main.ts
│   ├───ngsw-config.json               # config service worker
│   ├───polyfills.ts
│   ├───root-routing.module.ts         # module root App
│   ├───root.component.ts
│   ├───root.module.ts
│   ├───test.ts
│   ├───tsconfig.app.json
│   ├───tsconfig.json
│   └───typings.d.ts
├───.angular-cli.json
├───.editorconfig
├───.gitignore
├───karma.conf.js
├───package-lock.json
├───package.json
├───protractor.conf.js
├───README.md
└───tslint.json
```