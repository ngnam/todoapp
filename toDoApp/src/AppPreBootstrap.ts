import 'rxjs/add/operator/toPromise';

import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConsts } from '@shared/AppConsts';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Type, CompilerOptions, NgModuleRef, Inject, Injectable } from '@angular/core';

@Injectable()
export class AppPreBootstrap {
    constructor(private httpClient: HttpClient) { 
    }

    static bootstrap<TM>(moduleType: Type<TM>, compilerOptions?: CompilerOptions | CompilerOptions[]): Promise<NgModuleRef<TM>> {
        return platformBrowserDynamic().bootstrapModule(moduleType, compilerOptions);
    }

    initializeApp(): Promise<any> {
        return new Promise((resolve, reject) => {
            console.log(`initializeApp:: inside promise`);
            setTimeout(() => {
                console.log(`initializeApp:: inside setTimeout`);
                // doing something
                resolve();
            }, 3000);
        });
    }

    getApplicationConfig(): Promise<any> {
        console.log(`getSettings:: before http.get call`);
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Accept": "application/json"
            })
        };
        const promise = this.httpClient.get('/assets/appconfig.json', options)
            .toPromise()
            .then((settings: any) => {
                console.log(`Settings from API: `, settings);
                AppConsts.appBaseUrl = settings.appBaseUrl;
                AppConsts.remoteServiceBaseUrl = settings.remoteServiceBaseUrl;
                console.log('Request Complete');
                return settings;
            });
        return promise;
    }
}