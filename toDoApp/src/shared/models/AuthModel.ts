export interface IAuthenticateModel {
    userName: string;
    passWord: string;
}

export class AuthenticateModel implements IAuthenticateModel {
    userName: string;
    passWord: string;

    constructor(data?: IAuthenticateModel) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.userName = data["userName"];
            this.passWord = data["passWord"];
        }
    }

    static fromJS(data: any): AuthenticateModel {
        let result = new AuthenticateModel();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["userName"] = this.userName;
        data["passWord"] = this.passWord;
        return data; 
    }

    clone() {
        const json = this.toJSON();
        let result = new AuthenticateModel();
        result.init(json);
        return result;
    }
}

export interface IAuthenticateResultModel {
    isAuth: boolean;
    tokeyType: string;
    tokenKey: any;
    time: any;
}

export class AuthenticateResultModel implements IAuthenticateResultModel {
    isAuth: boolean;
    tokeyType: string;
    tokenKey: any;
    time: any;

    constructor(data?: IAuthenticateResultModel) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.isAuth = data["isAuth"];
            this.tokeyType = data["tokeyType"];
            this.tokenKey = data["tokenKey"];
            this.time = data["time"];
        }
    }

    static fromJS(data: any): AuthenticateResultModel {
        let result = new AuthenticateResultModel();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["isAuth"] = this.isAuth;
        data["tokeyType"] = this.tokeyType;
        data["tokenKey"] = this.tokenKey;
        data["time"] = this.time;
        return data; 
    }

    clone() {
        const json = this.toJSON();
        let result = new AuthenticateResultModel();
        result.init(json);
        return result;
    }
}