import { JsonHelpers } from "@shared/helpers/json-helper";

export class UserInputDto implements IUserInputDto {
    userId?: any | undefined;
    userName: string;
    passWord: string;
    firstName: string;
    lastName: string;
    birthDay?: Date | undefined;
    tokenKey?: string | undefined;

    constructor(data?: IUserInputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any, _mappings?: any) {
        if (data) {
            this.userId = data["userId"];
            this.userName = data["userName"];
            this.passWord = data["passWord"];
            this.firstName = data["firstName"];
            this.lastName = data["lastName"];
            this.birthDay = data["birthDay"] ? new Date(data["birthDay"].toString()) : <any>undefined;
            this.tokenKey = data["tokenKey"];
        }
    }

    static fromJS(data: any, _mappings?: any): UserInputDto {
        data = typeof data === 'object' ? data : {};
        return JsonHelpers.createInstance<UserInputDto>(data, _mappings, UserInputDto);
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["userId"] = this.userId;
        data["userName"] = this.userName;
        data["passWord"] = this.passWord;
        data["firstName"] = this.firstName;
        data["lastName"] = this.lastName;
        data["birthDay"] = this.birthDay ? this.birthDay.toISOString() : <any>undefined;
        data["tokenKey"] = this.tokenKey;
        return data; 
    }
}

export interface IUserInputDto {
    userId?: any | undefined;
    userName: string;
    passWord: string;
    firstName: string;
    lastName: string;
    birthDay?: Date | undefined;
    tokenKey?: string | undefined;
}

