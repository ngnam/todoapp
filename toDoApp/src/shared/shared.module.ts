import { NgModule, ModuleWithProviders } from "@angular/core";
import { AppAuthService } from "@shared/auth/app-auth.service";
import { AppRouteGuard } from "@shared/auth/auth-route-guard";
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { TokenInterceptor } from "@shared/auth/token.interceptor";
import { JwtHelper } from 'angular2-jwt';
import { TokenService } from "@shared/auth/token-service";
import { UtilsService } from "@shared/utils/utils-service";
import { ClickOutsideDirective } from "@shared/directives/click-outside.directive";
import { ClickInsideDirective } from "@shared/directives/click-inside.directive";

@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],
    declarations: [
        ClickOutsideDirective,
        ClickInsideDirective
    ],
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                AppAuthService,
                AppRouteGuard,
                // request token will use when It used with api of real
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: TokenInterceptor,
                    multi: true
                },
                JwtHelper,
                TokenService,
                UtilsService
            ]
        }
    }
}
