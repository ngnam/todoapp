import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `<p><b>404</b> - Page not found</p>`
})
export class PageNotFoundComponent {}
