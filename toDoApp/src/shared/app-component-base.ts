import { Injector, ElementRef } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';

export abstract class AppComponentBase {
    elementRef: ElementRef;

    constructor(injector: Injector) {
        this.elementRef = injector.get(ElementRef);
    }
}
