export class AppConsts {
    static remoteServiceBaseUrl: string;
    static appBaseUrl: string;

    static readonly authorization = {
        encrptedAuthTokenName: 'enc_auth_token'
    };
}