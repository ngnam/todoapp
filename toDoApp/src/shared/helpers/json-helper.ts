export class JsonHelpers {
    static createInstance<T>(data: any, mappings: any, type: any): T {
        if (!mappings)
            mappings = [];
        else {
            let mapping = mappings.filter((m: any) => m.source === data);
            if (mapping.length === 1)
                return <T>mapping[0].target;
        }
    
        let result: any = new type();
        mappings.push({ source: data, target: result });
        result.init(data, mappings);
        return result;
    }
}