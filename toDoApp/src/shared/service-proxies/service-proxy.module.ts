import { NgModule } from '@angular/core';
// 
import * as ApiServiceProxies from './service-proxies';
// import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
// import { MockDataTodoService } from '@shared/service-proxies/mockdata.todo.service';
import { fakeBackendProvider } from '@shared/service-proxies/fake-backend-interceptor.module';

@NgModule({
    // use if is memory api 
    // imports: [
    //     InMemoryWebApiModule.forRoot(MockDataTodoService, { dataEncapsulation: false, delay: 500 }),
    // ],
    providers: [
        // provider used to create fake backend
        // remove if used with api real
        fakeBackendProvider,
        ApiServiceProxies.UserServiceProxy,
    ]
})
export class ServiceProxyModule { }