import { InMemoryDbService } from "angular-in-memory-web-api";
import { UserInputDto } from "@shared/models/user-model";

export class MockDataTodoService implements InMemoryDbService {
    createDb() {
        const users = [
            new UserInputDto({"userId": 1, "userName": "admin", "passWord": "admin", "firstName": "Admin", "lastName": "1", "birthDay": null, "tokenKey": null}),
            new UserInputDto({"userId": 2, "userName": "admin1", "passWord": "admin", "firstName": "Admin", "lastName": "2", "birthDay": null, "tokenKey": null}),
            new UserInputDto({"userId": 3, "userName": "admin2", "passWord": "admin", "firstName": "Admin", "lastName": "3", "birthDay": null, "tokenKey": null}),
            new UserInputDto({"userId": 4, "userName": "admin3", "passWord": "admin", "firstName": "Admin", "lastName": "4", "birthDay": null, "tokenKey": null}),
            new UserInputDto({"userId": 5, "userName": "admin4", "passWord": "admin", "firstName": "Admin", "lastName": "5", "birthDay": null, "tokenKey": null}),
            new UserInputDto({"userId": 6, "userName": "admin5", "passWord": "admin", "firstName": "Admin", "lastName": "6", "birthDay": null, "tokenKey": null}),
            new UserInputDto({"userId": 7, "userName": "admin6", "passWord": "admin", "firstName": "Admin", "lastName": "7", "birthDay": null, "tokenKey": null})
        ];

        return {
            account: users,
        }
    }
}