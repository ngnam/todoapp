import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

import { Observable } from 'rxjs/Observable';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpResponseBase, HttpErrorResponse } from '@angular/common/http';
import { UserInputDto } from '@shared/models/user-model';

export const API_BASE_URL = new InjectionToken<string>('API_BASE_URL');

export interface IUserServiceProxy {
    /**
     * @skip (optional) 
     * @take (optional) 
     * @query (optional) 
     * @return Success
     */
    usersGet(skip?: number | null | undefined, take?: number | null | undefined, query?: string | null | undefined): Observable<any>;
    /**
     * @model (optional) 
     * @return Success
     */
    usersPost(model?: UserInputDto | null | undefined): Observable<void>;
    /**
     * @return Success
     */
    usersByIdGet(id: string): Observable<void>;
    /**
     * @model (optional) 
     * @return Success
     */
    usersByIdPut(id: string, model?: UserInputDto | null | undefined): Observable<void>;
    /**
     * @return Success
     */
    usersByIdDelete(id: string): Observable<void>;
    /**
     * @model (optional) 
     * @return Success
     */
    usersLoginPost(model?: LoginDto | null | undefined): Observable<void>;
}

@Injectable()
export class UserServiceProxy implements IUserServiceProxy {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    /**
     * @skip (optional) 
     * @take (optional) 
     * @query (optional) 
     * @return Success
     */

    usersGet(skip?: number | null | undefined, take?: number | null | undefined, query?: string | null | undefined): Observable<any> {
        let url_ = this.baseUrl + "/api/Users?";
        if (skip !== undefined)
            url_ += "skip=" + encodeURIComponent("" + skip) + "&"; 
        if (take !== undefined)
            url_ += "take=" + encodeURIComponent("" + take) + "&"; 
        if (query !== undefined)
            url_ += "query=" + encodeURIComponent("" + query) + "&"; 
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            headers: new HttpHeaders({
                "Content-Type": "application/json", 
                "Accept": "application/json"
            })
        };

        return this.http.request("get", url_, options_);
    }

    protected processUsersGet(response: HttpResponseBase): Observable<any> {
        const status = response.status;
        const responseBlob = 
            response instanceof HttpResponse ? response.body : 
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }};
        if (status === 200) {
            return blobToText(responseBlob).flatMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : jsonParse(_responseText, this.jsonParseReviver);
            if (resultData200) {
                result200 = {};
                for (let key in resultData200) {
                    if (resultData200.hasOwnProperty(key))
                        result200[key] = resultData200[key];
                }
            }
            return Observable.of(result200);
            });
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Observable.of<any>(<any>null);
    }

    /**
     * @model (optional) 
     * @return Success
     */
    usersPost(model?: UserInputDto | null | undefined): Observable<void> {
        let url_ = this.baseUrl + "/api/Users";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(model);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json", 
            })
        };

        return this.http.request("post", url_, options_).flatMap((response_ : any) => {
            return this.processUsersPost(response_);
        }).catch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processUsersPost(<any>response_);
                } catch (e) {
                    return <Observable<void>><any>Observable.throw(e);
                }
            } else
                return <Observable<void>><any>Observable.throw(response_);
        });
    }

    protected processUsersPost(response: HttpResponseBase): Observable<void> {
        const status = response.status;
        const responseBlob = 
            response instanceof HttpResponse ? response.body : 
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }};
        if (status === 200) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return Observable.of<void>(<any>null);
            });
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Observable.of<void>(<any>null);
    }

    /**
     * @return Success
     */
    usersByIdGet(id: string): Observable<void> {
        let url_ = this.baseUrl + "/api/Users/{id}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id)); 
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json", 
            })
        };

        return this.http.request("get", url_, options_).flatMap((response_ : any) => {
            return this.processUsersByIdGet(response_);
        }).catch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processUsersByIdGet(<any>response_);
                } catch (e) {
                    return <Observable<void>><any>Observable.throw(e);
                }
            } else
                return <Observable<void>><any>Observable.throw(response_);
        });
    }

    protected processUsersByIdGet(response: HttpResponseBase): Observable<void> {
        const status = response.status;
        const responseBlob = 
            response instanceof HttpResponse ? response.body : 
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }};
        if (status === 200) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return Observable.of<void>(<any>null);
            });
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Observable.of<void>(<any>null);
    }

    /**
     * @model (optional) 
     * @return Success
     */
    usersByIdPut(id: string, model?: UserInputDto | null | undefined): Observable<void> {
        let url_ = this.baseUrl + "/api/Users/{id}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id)); 
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(model);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json", 
            })
        };

        return this.http.request("put", url_, options_).flatMap((response_ : any) => {
            return this.processUsersByIdPut(response_);
        }).catch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processUsersByIdPut(<any>response_);
                } catch (e) {
                    return <Observable<void>><any>Observable.throw(e);
                }
            } else
                return <Observable<void>><any>Observable.throw(response_);
        });
    }

    protected processUsersByIdPut(response: HttpResponseBase): Observable<void> {
        const status = response.status;
        const responseBlob = 
            response instanceof HttpResponse ? response.body : 
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }};
        if (status === 200) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return Observable.of<void>(<any>null);
            });
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Observable.of<void>(<any>null);
    }

    /**
     * @return Success
     */
    usersByIdDelete(id: string): Observable<void> {
        let url_ = this.baseUrl + "/api/Users/{id}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id)); 
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json", 
            })
        };

        return this.http.request("delete", url_, options_).flatMap((response_ : any) => {
            return this.processUsersByIdDelete(response_);
        }).catch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processUsersByIdDelete(<any>response_);
                } catch (e) {
                    return <Observable<void>><any>Observable.throw(e);
                }
            } else
                return <Observable<void>><any>Observable.throw(response_);
        });
    }

    protected processUsersByIdDelete(response: HttpResponseBase): Observable<void> {
        const status = response.status;
        const responseBlob = 
            response instanceof HttpResponse ? response.body : 
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }};
        if (status === 200) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return Observable.of<void>(<any>null);
            });
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Observable.of<void>(<any>null);
    }

    /**
     * @model (optional) 
     * @return Success
     */
    usersLoginPost(model?: LoginDto | null | undefined): Observable<void> {
        let url_ = this.baseUrl + "/api/Users/login";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(model);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json", 
            })
        };

        return this.http.request("post", url_, options_).flatMap((response_ : any) => {
            return this.processUsersLoginPost(response_);
        }).catch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processUsersLoginPost(<any>response_);
                } catch (e) {
                    return <Observable<void>><any>Observable.throw(e);
                }
            } else
                return <Observable<void>><any>Observable.throw(response_);
        });
    }

    protected processUsersLoginPost(response: HttpResponseBase): Observable<void> {
        const status = response.status;
        const responseBlob = 
            response instanceof HttpResponse ? response.body : 
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }};
        if (status === 200) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return Observable.of<void>(<any>null);
            });
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).flatMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            });
        }
        return Observable.of<void>(<any>null);
    }
}

export class LoginDto implements ILoginDto {
    userName: string;
    passWord: string;

    constructor(data?: ILoginDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any, _mappings?: any) {
        if (data) {
            this.userName = data["userName"];
            this.passWord = data["passWord"];
        }
    }

    static fromJS(data: any, _mappings?: any): LoginDto {
        data = typeof data === 'object' ? data : {};
        return createInstance<LoginDto>(data, _mappings, LoginDto);
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["userName"] = this.userName;
        data["passWord"] = this.passWord;
        return data; 
    }
}

export interface ILoginDto {
    userName: string;
    passWord: string;
}

export class PagedResultDtoOfUserDto implements IPagedResultDtoOfUserDto {
    result?: any | undefined;
    modelState?: any | undefined;
    errors?: any[] | undefined;

    constructor(data?: IPagedResultDtoOfUserDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any, _mappings?: any) {
        if (data) {
            if (data["result"]) {
                this.result = {};
                for (let key in data["result"]) {
                    if (data["result"].hasOwnProperty(key))
                        this.result[key] = data["result"][key];
                }
            }
            if (data["modelState"]) {
                this.modelState = {};
                for (let key in data["modelState"]) {
                    if (data["modelState"].hasOwnProperty(key))
                        this.modelState[key] = data["modelState"][key];
                }
            }
            if (data["errors"] && data["errors"].constructor === Array) {
                this.errors = [];
                for (let item of data["errors"])
                    this.errors.push(item);
            }
        }
    }

    static fromJS(data: any, _mappings?: any): PagedResultDtoOfUserDto {
        data = typeof data === 'object' ? data : {};
        return createInstance<PagedResultDtoOfUserDto>(data, _mappings, PagedResultDtoOfUserDto);
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (this.result) {
            data["result"] = {};
            for (let key in this.result) {
                if (this.result.hasOwnProperty(key))
                    data["result"][key] = this.result[key];
            }
        }
        if (this.modelState) {
            data["modelState"] = {};
            for (let key in this.modelState) {
                if (this.modelState.hasOwnProperty(key))
                    data["modelState"][key] = this.modelState[key];
            }
        }
        if (this.errors && this.errors.constructor === Array) {
            data["errors"] = [];
            for (let item of this.errors)
                data["errors"].push(item);
        }
        return data; 
    }
}

export interface IPagedResultDtoOfUserDto {
    result?: any | undefined;
    modelState?: any | undefined;
    errors?: any[] | undefined;
}

function jsonParse(json: any, reviver?: any) {
    json = JSON.parse(json, reviver);

    var byid: any = {};
    var refs: any = [];
    json = (function recurse(obj: any, prop?: any, parent?: any) {
        if (typeof obj !== 'object' || !obj)
            return obj;
        
        if ("$ref" in obj) {
            let ref = obj.$ref;
            if (ref in byid)
                return byid[ref];
            refs.push([parent, prop, ref]);
            return undefined;
        } else if ("$id" in obj) {
            let id = obj.$id;
            delete obj.$id;
            if ("$values" in obj)
                obj = obj.$values;
            byid[id] = obj;
        }
        
        if (Array.isArray(obj)) {
            obj = obj.map((v, i) => recurse(v, i, obj));
        } else {
            for (var p in obj) {
                if (obj.hasOwnProperty(p) && obj[p] && typeof obj[p] === 'object')
                    obj[p] = recurse(obj[p], p, obj);
            }
        }

        return obj;
    })(json);

    for (let i = 0; i < refs.length; i++) {
        const ref = refs[i];
        ref[0][ref[1]] = byid[ref[2]];
    }

    return json;
}

function createInstance<T>(data: any, mappings: any, type: any): T {
    if (!mappings)
        mappings = [];
    else {
        let mapping = mappings.filter((m: any) => m.source === data);
        if (mapping.length === 1)
            return <T>mapping[0].target;
    }

    let result: any = new type();
    mappings.push({ source: data, target: result });
    result.init(data, mappings);
    return result;
}

export class SwaggerException extends Error {
    message: string;
    status: number; 
    response: string; 
    headers: { [key: string]: any; };
    result: any; 

    constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
        super();

        this.message = message;
        this.status = status;
        this.response = response;
        this.headers = headers;
        this.result = result;
    }

    protected isSwaggerException = true;

    static isSwaggerException(obj: any): obj is SwaggerException {
        return obj.isSwaggerException === true;
    }
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
    if(result !== null && result !== undefined)
        return Observable.throw(result);
    else
        return Observable.throw(new SwaggerException(message, status, response, headers, null));
}

function blobToText(blob: any): Observable<string> {
    return new Observable<string>((observer: any) => {
        if (!blob) {
            observer.next("");
            observer.complete();
        } else {
            let reader = new FileReader(); 
            reader.onload = function() { 
                observer.next(this.result);
                observer.complete();
            }
            reader.readAsText(blob); 
        }
    });
}