import { Injectable } from '@angular/core';

export interface ITokenService {
    getToken(): any;
    clearToken(): void;
    setToken(authToken: string, authType?: string): void;
}

@Injectable()
export class TokenService implements ITokenService{

    getToken(): any {
        var auth = localStorage.getItem('authToken');
        if(auth == null || auth == undefined)
            return null;
        return JSON.parse(auth);
    }

    clearToken(): void {
       localStorage.removeItem("authToken");
       localStorage.clear();
    }

    setToken(authToken: string, authType?: string): void {
        var json = new Auth({ "token": authToken, "type": authType});
        localStorage.setItem("authToken", JSON.stringify(json));
    }
}

export class Auth implements IAuth{
    token: string;
    type: string;
    constructor(data?: IAuth) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.token = data["token"];
            this.type = data["type"];
        }
    }   
}

export interface IAuth {
    token: string;
    type: string;
}
