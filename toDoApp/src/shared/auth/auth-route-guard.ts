import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { AppAuthService as Auth } from '@shared/auth/app-auth.service';
@Injectable()
export class AppRouteGuard implements CanActivate {

    constructor(private router: Router, private auth: Auth) { }

    canActivate() {
        if (this.auth.isAuthenticated()) {
            // logged in so return true
            // this.router.navigate(['/app/users']);
            return true;
        } else {
            // not logged in so redirect to login page with the return url
            this.router.navigate(['account/login']);
            return false;
        }
    }
}