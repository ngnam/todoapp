import { Injectable } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import decode from 'jwt-decode';
import { tokenNotExpired } from 'angular2-jwt';
import { TokenService } from '@shared/auth/token-service';

@Injectable()
export class AppAuthService {

    constructor(private _tokenService: TokenService) {
    }

    public isAuthenticated(): boolean {
        // get the token
        const json = this._tokenService.getToken();
        if (json == null) return false;
        else {
            return true;
        }
    }

    public getToken(): any {
        const json = this._tokenService.getToken();
        if (json == null) return "";
        else {
            return json.token;
        }
    }

}