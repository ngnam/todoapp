import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(public auth: AppAuthService, private router: Router) { }
    // send request with authen token
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.auth.getToken()}` 
                // 'Bearer fake-jwt-token' Use with fake backend
                // `Bearer ${this.auth.getToken()}` Use with api real
            }
        });

        return next.handle(request);        
    }
}