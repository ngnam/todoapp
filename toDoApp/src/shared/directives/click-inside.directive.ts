import {Directive, ElementRef, Output, EventEmitter, HostListener} from '@angular/core';

@Directive({
    selector: '[clickInside]'
})
export class ClickInsideDirective {
    constructor(private _elementRef: ElementRef) {
    }

    @Output()
    public clickInside = new EventEmitter<MouseEvent>();

    @HostListener('document:click', ['$event', '$event.target'])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        if (!targetElement) {
            return;
        }
        let name = targetElement.getAttribute('name') || targetElement.getAttribute('id') || targetElement.getAttribute('class');
        let target = targetElement;

        console.log(name, target);
        this.clickInside.emit(event);
    }
}