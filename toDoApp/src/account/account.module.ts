﻿import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AccountRoutingModule } from './account-routing.module';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';

import { SharedModule } from '@shared/shared.module';

import { AccountComponent } from './account.component';
import { LoginComponent } from './login/login.component';

import { LoginService } from './login/login.service';
import { DxFormModule, DxTextBoxModule, DxButtonModule } from 'devextreme-angular';

const MODULES = [  
    DxFormModule,
    DxTextBoxModule,
    DxButtonModule
];
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        SharedModule,
        ServiceProxyModule,
        AccountRoutingModule,
        ...MODULES
    ],
    declarations: [
        AccountComponent,
        LoginComponent,
        
    ],
    providers: [
        LoginService
    ]
})
export class AccountModule {

}
