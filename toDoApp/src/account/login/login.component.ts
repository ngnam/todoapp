﻿import { ILoginDto } from './../../shared/service-proxies/service-proxies';
import { Component, Injector, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/app-component-base';
import { LoginService } from './login.service';
import { AuthenticateModel, AuthenticateResultModel } from '@shared/models/AuthModel';
import { DxFormComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';

@Component({
    template: `
<div id="form-container">
    <form name="LoginFrm">
        <dx-form id="LoginAreaForm" #LoginAreaForm [(formData)]="loginModel" labelLocation="left" [readOnly]="false" [screenByWidth]="getSizeScreen"
            validationGroup="LoginAreaFormValidate">
            <dxi-item itemType="group" caption="LogIn">
                <dxi-item dataField="userName" editorType="dxTextBox" [editorOptions]="{ name: 'input-userName', width: '100%' }">
                    <dxo-label text="User Name"></dxo-label>
                    <dxi-validation-rule type="required" message="The UserName field is required."></dxi-validation-rule>
                </dxi-item>
                <dxi-item dataField="passWord" editorType="dxTextBox" [editorOptions]="{ name: 'input-passWord', width: '100%', mode: 'password' }">
                    <dxo-label text="PassWord"></dxo-label>
                    <dxi-validation-rule type="required" message="The PassWord field is required."></dxi-validation-rule>
                </dxi-item>
            </dxi-item>
        </dx-form>
        <dx-button id="btnSubmitLogin" name="btnSubmitLogin" text="Login" (onClick)="login($event)" icon="fa fa-sign-in">
        </dx-button>
    </form>
</div>
    `,
    styles: [`
#form-container {
    padding: 15px;
}
    
.float-right {
    float: right;
} 
`],
})
export class LoginComponent extends AppComponentBase {
    @ViewChild(DxFormComponent) LoginAreaForm: DxFormComponent;
    loginModel: AuthenticateModel;
    authenticateResult: AuthenticateResultModel;
    submitting: boolean = false;
    colCountByScreen: Object;
    constructor(
        injector: Injector,
        public loginService: LoginService,
        private _router: Router,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.loginModel = new AuthenticateModel({ "userName": null, "passWord": null });
        this.colCountByScreen = {
            xs: 1,
            sm: 3,
            md: 3,
            lg: 3
        };
        this.logout();
    }

    getSizeScreen(width) {
        if (width < 768) return "xs";
        if (width < 992) return "sm";
        if (width < 1200) return "md";
        return "lg";
    }

    login(e): void {
        console.log(this.loginModel);
        let validateForm = this.LoginAreaForm.instance.validate();
        if (!validateForm.isValid) {
            let message = validateForm.brokenRules.map(o => o.message).join(', ');
            return notify(message, "error")
        } else {
            this.loginService.authenticate(this.loginModel);
            this.submitting = true;
        }
    }

    logout(): void {
        this.loginService.unAuthenticate();
    }

}
