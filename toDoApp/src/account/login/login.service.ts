﻿import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConsts } from "@shared/AppConsts";
import { AuthenticateModel, AuthenticateResultModel } from "@shared/models/AuthModel";
import { Observable } from "rxjs/Observable";
import { TokenService } from "@shared/auth/token-service";
import { UtilsService } from "@shared/utils/utils-service";
import notify from "devextreme/ui/notify";

@Injectable()
export class LoginService {
    constructor(private _httpClient: HttpClient, private _router: Router, private _tokenService: TokenService, private _utilsService: UtilsService) {
    }

    authenticate = (authenticateModel: AuthenticateModel) => {
        let url = `${AppConsts.remoteServiceBaseUrl}/api/authenticate`;
        const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Accept": "application/json"
        })
        this._httpClient.post<any>(url, authenticateModel, {headers}).subscribe(data => {
            console.log(JSON.parse(data));
            this._tokenService.clearToken();
            this.processAuthenticateResult(JSON.parse(data));
        }, (error: any) => {
            console.log(error);
            notify('User Name or PassWord Incorect!', 'error');
        });
    }   

    unAuthenticate = () => {
        this._tokenService.clearToken();
        this._router.navigate(['account/login']);
    }

    private processAuthenticateResult(authenticateResult: AuthenticateResultModel) {
        if (authenticateResult.isAuth) {
            //Successfully logged in
            this.processLogin(authenticateResult);

        } else {
            console.log('Unexpected authenticateResult!');
            notify('User Name or PassWord Incorect!', 'error');
        }
    }

    private processLogin(authenticateResult: AuthenticateResultModel): void {
        this._tokenService.setToken(
            authenticateResult.tokenKey,
            authenticateResult.tokeyType
        );
        this._router.navigate(['app/users']);
    }
}