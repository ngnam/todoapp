﻿import { Component, ViewContainerRef, OnInit, ViewEncapsulation, Injector } from '@angular/core';
import { LoginService } from './login/login.service';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
    template: `
<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);">
            <img src='/assets/images/logo.png' />
            <span>Aventis</span>
        </a>
    </div>

    <router-outlet></router-outlet>

    <div class="box-footer">
        <div class="col-xs-12 text-center" style="color: #e9e9e9">
            2018
        </div>
    </div>
</div>`,
    encapsulation: ViewEncapsulation.None,
    styleUrls: [`account.component.scss`]
})
export class AccountComponent extends AppComponentBase implements OnInit {

    private viewContainerRef: ViewContainerRef;

    versionText: string;
    currentYear: number;

    public constructor(
        injector: Injector,
        private _loginService: LoginService
    ) {
        super(injector);

        this.currentYear = new Date().getFullYear();
    }

    showTenantChange(): boolean {
        return true;
    }

    ngOnInit(): void {
    }
}