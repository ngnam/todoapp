import { RootComponent } from "./root.component";
import { NgModule, Injector, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { SharedModule } from "@shared/shared.module";
import { ServiceProxyModule } from "@shared/service-proxies/service-proxy.module";
import { RootRoutingModule } from "./root-routing.module";
import { API_BASE_URL } from "@shared/service-proxies/service-proxies";
import { HttpClientModule } from "@angular/common/http";
import { AppPreBootstrap } from "./AppPreBootstrap";
import { AppConsts } from "@shared/AppConsts";
import { PageNotFoundComponent } from "@shared/page-not-found/page-not-found.component";
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from "./environments/environment";

export function getRemoteServiceBaseUrl(): string {
    return AppConsts.remoteServiceBaseUrl;
}

export function appInitializerFactory(appPreBootstrap: AppPreBootstrap) {
    return () => appPreBootstrap.initializeApp();
}

export function getApplicationConfig(appPreBootstrap: AppPreBootstrap) {
    return () => {
        appPreBootstrap.getApplicationConfig();
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        BrowserModule,
        SharedModule.forRoot(),
        ServiceProxyModule,
        RootRoutingModule,
        ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
    ],
    declarations: [
        RootComponent,
        PageNotFoundComponent
    ],
    providers: [
        AppPreBootstrap,
        {
            provide: APP_INITIALIZER,
            useFactory: appInitializerFactory,
            deps: [AppPreBootstrap],
            multi: true
        },
        {
            provide: APP_INITIALIZER,
            useFactory: getApplicationConfig,
            deps: [AppPreBootstrap],
            multi: true
        },
        { provide: API_BASE_URL, useFactory: getRemoteServiceBaseUrl },        
    ],
    bootstrap: [RootComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class RootModule {

}
