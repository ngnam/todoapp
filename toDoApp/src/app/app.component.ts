import { Component, ViewContainerRef, Injector, OnInit, AfterViewInit, ViewChild, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/app-component-base';
import { Router } from '@angular/router';

@Component({
  template: `
<div class="wrapper" #wrapper>
    <app-header [fixed]="toggle" (clickInside)="clickInside()" (onChange$)="parentCountChanges($event)" (onClick$)="parentHandleClick($event)"
        (clickOutside)="parentHandleOutClick()"></app-header>
    <div class="tools-panel">
        <div class="tools-panel-content content" *ngIf="!checkLogin()">
            <p class="user-ico">
                <i class="fa fa-user" style="color: #F4BA2C;"></i>
            </p>
            <p class="user-logout">
                <a [routerLink]="['/account/login']">
                    <b>Hi: Admin,</b> Logout</a>
            </p>
        </div>
    </div>
    <div class="body" #widgetParentDiv>
        <app-aside></app-aside>
        <router-outlet></router-outlet>
    </div>
</div>`
})

export class AppComponent extends AppComponentBase implements AfterViewInit {
  divWidth = 0;
  toggle = false;
  private viewContainerRef: ViewContainerRef;
  @ViewChild('wrapper') wrapper: ElementRef;
  @ViewChild('widgetParentDiv') parentDiv: ElementRef;
  @HostListener('window:resize', ['$event'])
  onResize(event) {    
    if (this.parentDiv) {
      this.divWidth = this.parentDiv.nativeElement.clientWidth;
      this.toggleSelector(this.parentDiv, this.divWidth);
    }
  }

  constructor(
    injector: Injector,
    private _renderer: Renderer2, private _router: Router
  ) {
    super(injector);
    console.clear();
  }

  ngAfterViewInit(): void {
    // wait a tick to avoid one-time devMode
    setTimeout(_ => {
      this.divWidth = this.parentDiv.nativeElement.clientWidth
      this.toggleSelector(this.parentDiv, this.divWidth);
    });
  }

  checkLogin = () => {
    return window.location.hash.indexOf("/login") > -1;
  }

  private toggleSelector(el: ElementRef, divWidth: number) {
    let sidebar = el.nativeElement.querySelector('.sidebar-left');
    let viewcontent = el.nativeElement.querySelector('section.view-content');
    console.log(sidebar);
    console.log(viewcontent);
    if (sidebar == undefined || viewcontent == undefined)
      return;
    if (divWidth <= 768) {
      this._renderer.addClass(sidebar, "margin-left-300-l");
      setTimeout(_ => this._renderer.addClass(viewcontent, "margin-left-0"));
    } else {
      this._renderer.removeClass(viewcontent, "margin-left-0");
      setTimeout(_ => this._renderer.removeClass(sidebar, "margin-left-300-l"));
    }
  }

  parentHandleClick($event) {
    let sidebar = this.parentDiv.nativeElement.querySelector('.sidebar-left');
    let viewcontent = this.parentDiv.nativeElement.querySelector('section.view-content');
    if (this.toggle) {
      this._renderer.addClass(sidebar, "margin-left-300-l");
      setTimeout(_ => this._renderer.addClass(viewcontent, "margin-left-0"));
    } else {
      this._renderer.removeClass(viewcontent, "margin-left-0");
      setTimeout(_ => this._renderer.removeClass(sidebar, "margin-left-300-l"));
    }
  }

  parentHandleOutClick() {
    if (!this.toggle)
      return;
    this.toggle = !this.toggle;
    let sidebar = this.parentDiv.nativeElement.querySelector('.sidebar-left');
    let viewcontent = this.parentDiv.nativeElement.querySelector('section.view-content');
    this._renderer.addClass(sidebar, "margin-left-300-l");
    setTimeout(_ => this._renderer.addClass(viewcontent, "margin-left-0"));
  }

  parentCountChanges(event) {
    this.toggle = event;
  }

  clickInside() {
  }
}