﻿import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { UserServiceProxy } from '@shared/service-proxies/service-proxies';
import notify from 'devextreme/ui/notify';
import { UserInputDto } from '@shared/models/user-model';

@Component({
    template: `
<section class="view-content">
    <div class="content">
        <div class="content-header">
            <h3>Users Manager</h3>
        </div>
        <div class="content-body">
            <dx-scroll-view id="sc-gv-users" #scgvUsers height="auto" width="auto" direction="horizontal" showScrollbar="onHover" bounceEnabled="true"
                useNative="true">
                <dx-data-grid id="gv-users" #gvUsers [(dataSource)]="usersData" columnAutoWidth="true" [showRowLines]='true' [showBorders]="true"
                    rowTemplate="rowGvUersDataTemplate" [hoverStateEnabled]="true">
                    <dxi-column dataField="userId" [visible]="false"></dxi-column>
                    <dxi-column dataField="firstName" [allowSorting]="false" caption="First Name"></dxi-column>
                    <dxi-column dataField="lastName" [allowSorting]="false" caption="LastName"></dxi-column>
                    <dxi-column dataField="userName" [allowSorting]="false" caption="User Name"></dxi-column>
                    <dxi-column dataField="birthDay" [allowSorting]="false" caption="BirthDay"></dxi-column>

                    <dxo-selection mode="single"></dxo-selection>
                    <dxo-editing mode="row"></dxo-editing>
                    <dxo-paging [pageSize]="10"></dxo-paging>
                    <dxo-pager [showPageSizeSelector]="true" [allowedPageSizes]="[10, 20, 30]"></dxo-pager>

                    <tbody *dxTemplate="let item of 'rowGvUersDataTemplate'">
                        <tr class="dx-row dx-data-row dx-row-lines dx-column-lines">
                            <td>
                                <span>{{item.data.firstName}}</span>
                            </td>
                            <td>
                                <span>{{item.data.lastName}}</span>
                            </td>
                            <td>
                                <span>{{item.data.userName}}</span>
                            </td>
                            <td>
                                <span>{{item.data.birthDay | date: 'dd.MM.yyyy'}}</span>
                            </td>
                        </tr>
                    </tbody>
                </dx-data-grid>
            </dx-scroll-view>
        </div>
    </div>
</section>
    `
})
export class UsersComponent implements OnInit {

    ngOnInit(): void {
        this.list();
    }
    usersData: UserInputDto[] = [];

    constructor(
        injector: Injector,
        private _userService: UserServiceProxy
    ) {
    }

    protected list(): void {
        this._userService.usersGet()
            .subscribe((data: any) => {
                if(data && data.result && data.result.items) {
                    this.usersData = data.result.items;
                    console.log(data.result);
                }                
            }, error => {
                console.log(error);
                notify(error, "error");
            });
    }
}