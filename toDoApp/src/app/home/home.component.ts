
import { Component, Injector, AfterViewInit } from '@angular/core';

@Component({
    template: `
<section class="view-content">
    <div class="content">
        <div class="content-header">
            <h3>Dash board</h3>
        </div>
        <div class="content-body">
            <p>home</p>
        </div>
    </div>
</section>
    `
})
export class HomeComponent implements AfterViewInit {

    constructor(
        injector: Injector
    ) {
    }

    ngAfterViewInit(): void {
    }
}
