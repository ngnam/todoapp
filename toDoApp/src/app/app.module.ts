import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';

import { HomeComponent } from '@app/home/home.component';
import { UsersComponent } from '@app/users/users.component';
import { DxTreeViewModule, DxScrollViewModule, DxButtonModule, DxToolbarModule, DxDataGridModule } from 'devextreme-angular';
import { HeaderComponent } from '@app/layout/header/header.component';
import { AsideComponent } from '@app/layout/aside/aside.component';

const COMPONENTS = [
    AppComponent,
    HomeComponent,
    UsersComponent,
    HeaderComponent,
    AsideComponent
];

@NgModule({
    declarations: [
        ...COMPONENTS
    ],
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        AppRoutingModule,
        ServiceProxyModule,
        SharedModule,
        DxTreeViewModule,
        DxScrollViewModule,
        DxButtonModule,
        DxToolbarModule,
        DxScrollViewModule,
        DxDataGridModule
    ],
    providers: [

    ]
})
export class AppModule { }
