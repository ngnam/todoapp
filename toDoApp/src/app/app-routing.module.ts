import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { AppRouteGuard as AuthGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from '@app/home/home.component';
import { UsersComponent } from '@app/users/users.component';

const AppRoutes: Routes = [{
  path: '',
  component: AppComponent,
  children: [
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(AppRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
