import { Component, EventEmitter, Output, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
<!--header component-->
<header id="header" class="top header-primary">
    <div class="header-container">
      <div class="header-logo">
        <a href="#/" class="logo">
          <img src='/assets/images/logo.png' width="40" height="40" />
          <span>Aventis</span>
        </a>
      </div>
      <div class="header-menu">
        <input type="hidden" [value]="fixed">
        <span id="menubar" (click)="handleClick($event)">
          <i class="fa fa-2x" [ngClass]="!fixed ? 'fa-bars' : 'fa-angle-left'"></i>
        </span>
      </div>
    </div>
</header>
`
})
export class HeaderComponent {  
  constructor() {
  }
  @Input()
  fixed: boolean;

  @Output()
  onClick$: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

  @Output()
  onChange$: EventEmitter<boolean> = new EventEmitter<boolean>();

  public handleClick(event: MouseEvent) {
    this.fixed = !this.fixed;
    this.onClick$.emit(event);
    this.onChange$.emit(this.fixed);
  }

  disableClick($event) {
    $event.preventDefault();
  }  
}
