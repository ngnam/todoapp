import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TreeItem, MenuTreeService } from './menu-trees.service';
import { DxTreeViewComponent } from 'devextreme-angular';

@Component({
    selector: 'app-aside',
    template: `
<aside class="sidebar-left">
    <div class="content">
      <div class="content-header">
        <h3>Todos</h3>
      </div>
      <div class="content-body">
        <dx-scroll-view height="auto" width="auto" direction="both" showScrollbar="always" bounceEnabled="true" useNative="false">
          <dx-tree-view #menuTreeLeft id="menuTreeLeft" [(items)]="treeItems" selectByClick="true" selectionMode="single" dataStructure="plain" parentIdExpr="parentId"
            keyExpr="Id" displayExpr="name" (onItemClick)="selectItem($event)">
            <div *dxTemplate="let data of 'item'">
              <span><i class="fa fa-{{data.icon}}"></i> <span style="padding-left: 5px;" id="{{data.module}}">{{data.name}}</span></span>
            </div>
          </dx-tree-view>
        </dx-scroll-view>
      </div>
    </div>
</aside>`,
    styles: [
        `
#delete-menu {
    float: right;
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    text-align: center;
    width: 43px;
    height: 43px;
    padding: 6px;
    cursor: pointer;
}

#delete-menu:hover i {
    color: #007AA3;
}

`
    ],
    providers: [MenuTreeService]
})
export class AsideComponent {
    treeItems: TreeItem[];
    ShowBtnDelete: boolean = false;
    menuClick: TreeItem;
    constructor(private _service: MenuTreeService, private _router: Router) {
        this.treeItems = _service.getTree();
    }
    @ViewChild(DxTreeViewComponent) menuTreeLeft: DxTreeViewComponent;

    ngOnInit() {
        this.findMenuItem();
    }

    findMenuItem = () => {
        this._router.routerState.root.queryParams.subscribe(p => {
            let itemMenu = this.treeItems.find(item => {
                return item.Id == p.id
            });
            localStorage.setItem("currentMenu", JSON.stringify(itemMenu));
            console.log(itemMenu);
        });
    }

    selectItem(e) {
        localStorage.setItem("currentMenu", JSON.stringify(e.itemData));
        this.menuClick = e.itemData;
        if (e.itemData) {
            this._router.navigate([e.itemData.module], { queryParams: { id: e.itemData.Id } });
        }
    }
}
