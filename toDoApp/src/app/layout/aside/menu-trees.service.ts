import { Injectable } from '@angular/core';
export interface IMenuTreeService {
    getTree(): TreeItem[];
}

export interface ITreeItem {
    Id: number;
    name: string;
    module?: string; 
    expanded?: boolean;
    parentId?: number;
    icon?: string;
    isClick?: boolean;
}

export class TreeItem implements ITreeItem {
    Id: number;
    name: string;
    module?: string; 
    expanded?: boolean;
    parentId?: number;
    icon?: string;
    isClick?: boolean;
}

@Injectable()
export class MenuTreeService implements IMenuTreeService{   
    getTree(): TreeItem[] {
        const treeItems: TreeItem[] = [
            {
                Id: 1,
                name: 'Dashboard',
                module: '/app/home',
                expanded: true,
                icon: 'tachometer',
            },
            {
                Id: 2,
                name: 'Users',
                module: '/app/users',
                expanded: true,
                icon: 'users',
            }  
        ];
        return treeItems;
    }
}
