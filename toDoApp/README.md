# ToDoApp

## Development server

Run `npm run serve-dev` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
## Build

Run `npm run build-dev` to build the project. The build artifacts will be stored in the `dist/` directory.

### Documents 

* [Angular style guild](https://angular.io/guide/styleguide#overall-structural-guidelines)
* [Angular HttpInterceptor](https://angular.io/api/common/http/HttpInterceptor)
* [Angular jwt authentication](https://blog.angular-university.io/angular-jwt-authentication/)
* [Angular authentication](https://auth0.com/blog/angular-2-authentication/)
* [Angular2-jwt](https://github.com/auth0/angular2-jwt)
* [Angular authenticatiopn using the httpClient & HttpIntercaptors](https://ryanchenkie.com/angular-authentication-using-the-http-client-and-http-interceptors)
* [Fake Backend](http://jasonwatmore.com/post/2017/12/15/angular-5-mock-backend-example-for-backendless-development)


#scipts
```code
"scripts": {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build --prod",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e",
    "build-dev": "npm run build",
    "ngsw-config": "node_modules/.bin/ngsw-config dist src/ngsw-config.json",
    "serve-dev": "npm run build-dev && http-server dist -p 4200",
    "build-prod-ngsw": "ng build --prod && npm run ngsw-config",
    "serve-prod-ngsw": "npm run build-prod-ngsw && http-server dist -p 4200"
},
```

### Overall structural guidelines angular
